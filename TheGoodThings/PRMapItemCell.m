//
//  PRMapItemCell.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRMapItemCell.h"
#import "Constance.h"
#import "PRLocaltionCalculator.h"
#import "PRStorageFactory.h"
#import "PRStorageImpl.h"
#import "PRGoodThing.h"
#import <SDWebImage/UIImageView+WebCache.h>


@implementation PRMapItemCell {
    id<PRStorageImpl> storage;
    PRGoodThing * goodThing;
}

@synthesize mTitle = _mTitle;
@synthesize mAddress = _mAddress;
@synthesize mDistance = _mDistance;
@synthesize mImage = _mImage;
@synthesize mBookmarkBtn = _mBookmarkBtn;

- (IBAction)clickBookmark:(id)sender {
    BOOL enable = [storage isFavorRecord:goodThing.gidSting] == false;
    BOOL result = [storage setFavor:goodThing.gidSting json:[goodThing getOriginalJson] enable:enable];
    [self updateBookmark:result];
}

-(void)updateBookmark:(BOOL)enable{
    NSString * bookmarkName = enable ? IMG_BOOKMARK_ENABLE : IMG_BOOKMARK_DISABLE;
    UIImage * img = [UIImage imageNamed:bookmarkName];
    [self.mBookmarkBtn setBackgroundImage:img forState:UIControlStateNormal];
}

-(void)updateView:(PRGoodThing *)data location:(CLLocation *)currentLocation {
    storage = [PRStorageFactory create];
    goodThing = data;
    self.mTitle.text = goodThing.title;
    [self.mImage sd_setImageWithURL:[NSURL URLWithString:goodThing.listImageUrl]
                placeholderImage:[UIImage imageNamed:PLACEHOLDER_ICON]];
    self.mAddress.text = goodThing.address;
    self.mDistance.text = [PRLocaltionCalculator distance2string:[goodThing getDistance:currentLocation]];
    [self updateBookmark:[storage isFavorRecord:goodThing.gidSting]];
    self.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tbl_cell_selected.png"]];
}

@end
