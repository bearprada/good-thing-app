//
//  UIBarButtonItem+PRUIBarButtonItem.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/22.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "UIBarButtonItem+PRUIBarButtonItem.h"

@implementation UIBarButtonItem (PRUIBarButtonItem)

+ (UIBarButtonItem*)barItemWithImage:(UIImage*)image target:(id)target action:(SEL)action{
    
    UIBarButtonItem * button = [[UIBarButtonItem alloc] init];
    [button setImage:[UIImage imageNamed:@"btn_menu.png"]];
    return button;
}

@end
