//
//  PRLocaltionCalculator.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/15.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRLocaltionCalculator.h"

@implementation PRLocaltionCalculator

+ (double) calDistance:(CLLocation *)a b:(CLLocation *)b{
    if (a == nil || b == nil){
        return -1;
    } else {
        return [a distanceFromLocation:b];
    }
}

+ (NSString *) distance2string:(double) distance {
    if(distance==-1)
        return @"無法得知距離";
    else if(distance<1000)
        return [[NSString alloc] initWithFormat:@"%.1fm",distance];
    else
        return [[NSString alloc] initWithFormat:@"%.1fkm",distance/1000];
}
@end
