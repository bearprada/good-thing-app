//
//  PRGoodThing.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/21.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PRGoodThing : NSObject

@property (assign, nonatomic) long gid;
@property (assign, nonatomic) NSString * gidSting;
@property (assign, nonatomic) NSString * title;
@property (assign, nonatomic) NSString * story;
@property (assign, nonatomic) NSString * memo;
@property (assign, nonatomic) NSString * address;
@property (assign, nonatomic) NSString * titleImageUrl;
@property (assign, nonatomic) NSString * coverImageUrl;
@property (assign, nonatomic) NSString * listImageUrl;
@property (assign, nonatomic) NSString * businessTime;
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longtitude;
@property (assign, nonatomic) double distance;
@property (assign, nonatomic) BOOL isBigIssue;
@property (assign, nonatomic) BOOL canPost;
@property (assign, nonatomic) BOOL isFuzzyLocation;
@property (assign, nonatomic) NSArray * images;

-(id)initWithJson:(NSDictionary *)json;

-(NSDictionary *) getOriginalJson;
-(double) getDistance:(CLLocation *) currentLocation;

@end
