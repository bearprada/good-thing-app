//
//  PRChatCell.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/9/13.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRChatCell.h"
#import "PRMessage.h"

@implementation PRChatCell {
    NSDictionary * originalData;
}

@synthesize mMessage = _mMessage;
@synthesize mTime = _mTime;
@synthesize mUsername = _mUsername;

-(void) setData:(NSDictionary *)msg seq:(int)seq{
    originalData = msg;
    self.mTime.text = [[NSString alloc] initWithFormat:@"%ld", [[msg objectForKey:JSON_KEY_TIMESTAMP] longValue] ];
    self.mUsername.text  = [[NSString alloc] initWithFormat:@"#%d", seq];
    [self setMessage:[msg objectForKey:JSON_KEY_MSG]];
}

-(void) setMessage:(NSString *)message {
    int LIMIT = 20 - 2;
    if (message.length > LIMIT) {
        NSString * msg = [message substringWithRange:NSMakeRange(0, LIMIT)];
        self.mMessage.text = msg;
    } else {
        self.mMessage.text = message;
    }
}
@end
