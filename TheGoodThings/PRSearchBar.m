//
//  PRSearchBar.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/21.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRSearchBar.h"
#import <QuartzCore/QuartzCore.h>

@implementation PRSearchBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}


- (void)layoutSubviews {
    int len = [self.subviews count];
        for(int i=0 ; i< len; i++){
            id subview = [self.subviews objectAtIndex:i];
        if([subview isKindOfClass:[UITextField class]]) { //conform?
            UITextField * textField = subview;
            [textField setBackgroundColor:[UIColor whiteColor]];
            //        [searchField setBackground: [UIImage imageNamed:@"searchbar_bg.png"] ];
            textField.layer.cornerRadius=16.0f;
            textField.layer.masksToBounds=YES;
            textField.layer.borderColor=[[UIColor redColor]CGColor];
            textField.layer.borderWidth= 2.0f;
            
            [textField setBorderStyle:UITextBorderStyleNone];
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"icon_search.png"]];
            imageView.frame =  CGRectMake(imageView.frame.origin.x, imageView.frame.origin.y,
                                16,16);
            textField.leftView = imageView;
            
            [textField setKeyboardAppearance:UIKeyboardAppearanceAlert];
        }

        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0f && [subview isKindOfClass:[UIButton class]]){
            UIButton *cancelButton = (UIButton*)subview;
            cancelButton.enabled = YES;
            [cancelButton setTitle:@"" forState:UIControlStateNormal];
            [cancelButton setBackgroundImage:[UIImage imageNamed:@"searchbar_cancel.png"] forState:UIControlStateNormal];
        }
    }
    [super layoutSubviews];
}

@end
