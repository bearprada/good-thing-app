//
//  PRUtils.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/22.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRUtils.h"

@implementation PRUtils

+ (UIBarButtonItem *) generateDetailBarItem:(id)target selector:(SEL)selector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_menu.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_menu_selected.png"] forState:UIControlStateHighlighted];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

+ (UIBarButtonItem *) genBackBarItem:(id)target selector:(SEL)selector{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0.0f, 0.0f, 80.0f, 40.0f)];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
