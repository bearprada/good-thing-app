//
//  UIBarButtonItem+PRUIBarButtonItem.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/22.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (PRUIBarButtonItem)

+ (UIBarButtonItem*)barItemWithImage:(UIImage*)image target:(id)target action:(SEL)action;

@end
