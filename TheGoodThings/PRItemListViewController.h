//
//  PRItemListViewController.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#define CellIdentifier @"map_item"

@interface PRItemListViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UISearchDisplayDelegate,UIActionSheetDelegate, CLLocationManagerDelegate>

@property (assign,nonatomic) NSInteger catagoryId;
@property (assign,nonatomic) NSString * catagoryName;
@property (weak, nonatomic) IBOutlet UISearchBar *mSearchBar;

@end
