//
//  PRAppDelegate.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
