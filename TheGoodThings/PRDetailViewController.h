//
//  PRDetailViewController.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/2.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "PRGoodThing.h"

@interface PRDetailViewController : UIViewController<UIImagePickerControllerDelegate, UIActionSheetDelegate, MFMailComposeViewControllerDelegate,UINavigationControllerDelegate, UIScrollViewDelegate, UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) PRGoodThing * jsonData;

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UILabel *mTitle;
@property (weak, nonatomic) IBOutlet UILabel *mDistance;
@property (weak, nonatomic) IBOutlet UILabel *mBusinessTime;
@property (weak, nonatomic) IBOutlet UITableView *mChatTable;
@property (weak, nonatomic) IBOutlet UILabel *numCheckIn;
@property (weak, nonatomic) IBOutlet UILabel *numLike;
@property (weak, nonatomic) IBOutlet UITextView *mMemo;
@property (weak, nonatomic) IBOutlet UITextView *mStory;

@property (weak, nonatomic) IBOutlet UIScrollView *imgScrollView;
@property (weak, nonatomic) IBOutlet UIButton *mBookmark;
@property (weak, nonatomic) IBOutlet UIImageView *mImage;
@property (weak, nonatomic) IBOutlet UIButton *btnFbLike;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckIn;
@property (weak, nonatomic) IBOutlet UIButton *btnMap;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMessage;

- (IBAction)clickAddPhoto:(id)sender;
- (IBAction)showRoute:(id)sender;
- (IBAction)clickFbLike:(id)sender;
- (IBAction)clickFbCheckin:(id)sender;
- (IBAction)clickReportIssue:(id)sender;
- (IBAction)clickBookmark:(id)sender;
- (IBAction)clickAddMessage:(id)sender;

@end
