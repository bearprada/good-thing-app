//
//  PRNetworkManager.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/15.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PRNetworkManager : NSObject

+(void)requestGoodThingList:(int) catagoryId currentLocation:(CLLocation *)location complete:(void(^)(NSArray *))completeBlock;
+(void)requestTopStory:(CLLocation *)location complete:(void(^)(NSDictionary *))completeBlock;
+(void)requestLikeNumber:(int)rid complete:(void(^)(int number))completeBlock;
+(void)requestCheckinNumber:(int)rid complete:(void(^)(int number))completeBlock;
+(void)addLikeNumber:(int)rid installationId:(NSString *)installationId complete:(void(^)(int number))completeBlock error:(void(^)(int))errorBlock;
+(void)reportCheckin:(int)rid installationId:(NSString *)installationId checkinId:(NSString *)checkinId complete:(void(^)(int number))completeBlock error:(void(^)(int))errorBlock;
+(void)postMessage:(int)rid installationId:(NSString *)installationId message:(NSString *)msg complete:(void(^)(int number))completeBlock error:(void(^)(int num))errorBlock;
+(void)requestPosts:(int)rid complete:(void(^)(NSArray * posts))completeBlock error:(void(^)(NSString * err))errorBlock;
@end
