//
//  PRDetailViewController.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/2.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "PRDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "PRStorageFactory.h"
#import "PRLocaltionCalculator.h"
#import "PRStorageImpl.h"
#import "PRNetworkManager.h"
#import "PRImagePreviewViewController.h"
#import "Constance.h"
#import "UIAlertView+WithContext.h"
#import "PRDetailNavButton.h"
#import "PRBackNavButton.h"
#import "NearbyPlacesRequestResult.h"
#import "PRMessage.h"
#import "PRChatCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

#define PREVIEW_IMG_WIDTH 72.0f
#define PREVIEW_IMG_HEIGHT 72.0f
#define PREVIEW_IMG_GAP 3.0f

#define CELL_IDENTIFIER @"cell_msg"
#define READ_MORE @"繼續閱讀"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface PRDetailViewController () {
    PRDetailNavButton * mDetailNavButton;
    int currentSelectedImage;
    int checkInNumber;
    NSArray * datasource;
}

@end

@implementation PRDetailViewController{
    CLLocationManager *locationManager;
    CLLocation * currentLocation;
    
    UIAlertView * routeWarningAlert;
    UIAlertView * installGoogleMapAlert;
    UIAlertView * normalRouteWarningAlert;
    
    id<PRStorageImpl> storage;
}

@synthesize jsonData = _jsonData;

@synthesize mTitle = _mTitle;
@synthesize mImage = _mImage;
@synthesize mStory = _mStory;
@synthesize mMemo = _mMemo;
@synthesize mDistance = _mDistance;
@synthesize btnFbLike = _btnFbLike;
@synthesize btnCheckIn = _btnCheckIn;
@synthesize btnMap = _btnMap;
@synthesize imgScrollView = _imgScrollView;
@synthesize mBookmark = _mBookmark;
@synthesize numCheckIn = _numCheckIn;
@synthesize numLike = _numLike;
@synthesize btnAddMessage = _btnAddMessage;
@synthesize btnAddPhoto = _btnAddPhoto;
@synthesize mScrollView = _mScrollView;
@synthesize mBusinessTime = _mBusinessTime;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    storage = [PRStorageFactory create];
    self.mTitle.text = self.jsonData.title;
    [self setStory:self.jsonData.story];
    self.mBusinessTime.text = self.jsonData.businessTime;
    self.mMemo.text = self.jsonData.memo;

    [self.mImage setClipsToBounds:YES];
    NSString * url = self.jsonData.coverImageUrl;
    [self.mImage sd_setImageWithURL:[NSURL URLWithString:url]
            placeholderImage:[UIImage imageNamed:PLACEHOLDER_DETAIL_COVER]];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];

    [self mDistance].text = [PRLocaltionCalculator distance2string:[self.jsonData getDistance:currentLocation]];
    BOOL isLiked = [storage isLike:self.jsonData.gidSting];
    BOOL needDisable = (isLiked == false);
    [self.btnFbLike setEnabled:needDisable];

    [self updateBookmark:[storage isFavorRecord:self.jsonData.gidSting]];

    [PRNetworkManager requestLikeNumber:self.jsonData.gid complete:^(int n){
        //TODO add the result into local cache
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateFbNumber:n];
        });
    }];
    [PRNetworkManager requestCheckinNumber:self.jsonData.gid complete:^(int n){
        //TODO add the result into local cache
        checkInNumber = n;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateCheckinNum:n];
        });
    }];
    
    [self requestUpdateChats];

    NSArray * images = self.jsonData.images;
    for(int i = 0 ;i<[images count]; i++){
        [self generateImageView:i imageUrl:[images objectAtIndex:i]];
    }
    
    for(int i = [images count] ; i< DEFAULT_DISPLAY_IMAGE_NUM ; i++ ){
        [self generateAddPhotoImageView:i image:[UIImage imageNamed:IMG_ADD_PHOTO]];
    }
    
    self.imgScrollView.contentSize = CGSizeMake(PREVIEW_IMG_WIDTH *[images count] , PREVIEW_IMG_HEIGHT);
    
    /** 
     TODO replace the ui setting
     set click event on buttons */
    UIImage * clickedImage = [UIImage imageNamed:@"btn_bg_clicked"];
    UIImage * clickedImage2 = [UIImage imageNamed:@"btn_bg2_clicked"];
    
    [self.btnFbLike setImage:clickedImage forState:UIControlStateHighlighted];
    [self.btnFbLike setImage:clickedImage forState:UIControlStateDisabled];
    [self.btnMap setImage:clickedImage forState:UIControlStateHighlighted];
    [self.btnCheckIn setImage:clickedImage forState:UIControlStateHighlighted];
    [self.btnCheckIn setImage:clickedImage forState:UIControlStateDisabled];
    [self.btnAddPhoto setImage:clickedImage2 forState:UIControlStateHighlighted];
    [self.btnAddPhoto setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.btnAddMessage setImage:clickedImage2 forState:UIControlStateHighlighted];
    [self.btnAddMessage setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    mDetailNavButton = [[PRDetailNavButton alloc] initWithTarget:self];
    self.navigationItem.rightBarButtonItem = [mDetailNavButton generateDetailBarItem];
    self.navigationItem.leftBarButtonItem = [[[PRBackNavButton alloc] initWithTarget:self] generate:@selector(handleBack:)];
}

-(void) setStory:(NSString *)story {
    int LIMIT = 25 * 6 - 6; // FIXME it's not a good way to do this
    if (story.length >= LIMIT) {
        [self.mStory setText:[[NSString alloc] initWithFormat:@"%@...%@", [story substringWithRange:NSMakeRange(0, LIMIT)], READ_MORE]];
    } else {
//        [self.mStory setText:story];
        [self.mStory setText:[[NSString alloc] initWithFormat:@"%@...%@", story, READ_MORE]];
    }

    NSRange range=[self.mStory.text rangeOfString:READ_MORE];
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc]initWithString:self.mStory.text];
    [string addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xe21a49) range:range];
    [self.mStory setAttributedText:string];

    UITapGestureRecognizer* tapGestureObj = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapReadMore:)];
    tapGestureObj.numberOfTapsRequired = 1;
    [self.mStory addGestureRecognizer:tapGestureObj];
}

-(void)tapReadMore:(UITapGestureRecognizer*)gesture {
    NSString * msg = self.jsonData.story;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"攤販故事細節"
                                                    message:msg
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    // it's only work at iOS 4/5/6
    // http://stackoverflow.com/questions/19027083/align-message-in-uialertview-to-left-in-ios-7
    for (UIView *view in alert.subviews) {
        if([[view class] isSubclassOfClass:[UILabel class]]) {
            ((UILabel*)view).textAlignment = NSTextAlignmentLeft;
        }
    }
    [alert show];
}

-(void) requestUpdateChats {
    [PRNetworkManager requestPosts:self.jsonData.gid
                          complete:^(NSArray *posts){
                              datasource = posts;
                              [self performSelectorOnMainThread:@selector(updateChatTable) withObject:nil waitUntilDone:NO];
                              
                          }
                             error:^(NSString * err){
                                 //TODO show error message
                             }];
    
}

-(void)updateChatTable{
    [self.mChatTable reloadData];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.mScrollView.contentOffset = CGPointMake(0, 0);
    [self.mScrollView setContentOffset:CGPointZero animated:NO];
}

- (void)handleBack:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)clickNavDetail:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"好事地圖" delegate:mDetailNavButton cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"我的收藏", @"關於", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

-(void)generateImageView:(int) i imageUrl:(NSString *)url {
    CGRect frame;
    frame.origin.x = (PREVIEW_IMG_WIDTH+PREVIEW_IMG_GAP)*i;
    frame.origin.y = 0;
    frame.size = CGSizeMake(PREVIEW_IMG_WIDTH, PREVIEW_IMG_HEIGHT);
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:frame];
    imageview.tag = i;
    [imageview sd_setImageWithURL:[NSURL URLWithString:url]
                placeholderImage:[UIImage imageNamed:PLACEHOLDER_COVER]];
    [imageview setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previewImgClicked:)];
    [singleTap setNumberOfTapsRequired:1];
    [imageview addGestureRecognizer:singleTap];
    [self.imgScrollView addSubview:imageview];
}

-(void)generateAddPhotoImageView:(int) i image:(UIImage *)image {
    CGRect frame;
    frame.origin.x = (PREVIEW_IMG_WIDTH+PREVIEW_IMG_GAP)*i;
    frame.origin.y = 0;
    frame.size = CGSizeMake(PREVIEW_IMG_WIDTH, PREVIEW_IMG_HEIGHT);
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:frame];
    [imageview setImage:image];
    imageview.tag = i;
    [imageview setUserInteractionEnabled:YES];
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImgClicked:)];
    [singleTap setNumberOfTapsRequired:1];
    [imageview addGestureRecognizer:singleTap];
    [self.imgScrollView addSubview:imageview];
}

-(void)updateBookmark:(BOOL)enable {
    NSString * bookmarkName = enable ? IMG_BOOKMARK_ENABLE : IMG_BOOKMARK_DISABLE;
    UIImage * img = [UIImage imageNamed:bookmarkName];
    [self.mBookmark setBackgroundImage:img forState:UIControlStateNormal];
}

-(void)addImgClicked:(id)sender {
    [self launchActionSheetForAddPhoto];
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    currentLocation = locations[0];
    [locationManager stopUpdatingLocation];
}

-(void)launchActionSheetForAddPhoto {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"上傳照片" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"從相簿", @"從相機", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void) previewImgClicked:(id)sender {
    currentSelectedImage = [sender view].tag;
    [self performSegueWithIdentifier: @"preview_images" sender: self];
    //TODO show the detail images by full screen view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
   if([segue.identifier isEqualToString:@"preview_images"]){
        PRImagePreviewViewController *c = segue.destinationViewController;
        c.currentPage = currentSelectedImage;
        c.images = self.jsonData.images;
    }
}

- (IBAction)clickAddPhoto:(id)sender {
    [self launchActionSheetForAddPhoto];
}

/** TODO replace to the block parameter on UIAlertView */
- (IBAction)showRoute:(id)sender {
    double destLatitdue = self.jsonData.latitude;
    double destLongtitdue = self.jsonData.longtitude;
    [locationManager startUpdatingLocation];
    NSString * googleScheme = [[NSString alloc] initWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%f,%f&directionsmode=transit",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude,destLatitdue,destLongtitdue];
    NSURL * url  = [NSURL URLWithString:googleScheme];
    NSString * title = @"攤販位置可能不是很精準";
    NSString * confirm = @"確認";
    NSString * cancel = @"取消";
    if (![[UIApplication sharedApplication] canOpenURL:url]) {
         //left as an exercise for the reader: open the Google Maps mobile website instead!
         installGoogleMapAlert = [[UIAlertView alloc] initWithTitle:@"尚未安裝Google Map應用程式"
                            message:@"您必須要安裝該應用程式才能使用導引功能"
                            delegate:self
                            cancelButtonTitle:@"取消"
                            otherButtonTitles:@"前往App Store", nil];
        [installGoogleMapAlert show];
    } else {
        if (self.jsonData.isBigIssue) {
             routeWarningAlert = [[UIAlertView alloc] initWithTitle:title
                                                   message:@"若攤販無確切地址，則導引座標為大概位置。\n歡迎網友供更正確的資訊！\n注意：販售員可能因個人因素不在該販賣時間內出現。\n歡迎網友提供更正確的資訊！"
                                            delegate:self
                                            cancelButtonTitle:cancel
                                            otherButtonTitles:confirm, nil];
             routeWarningAlert.userInfo = [NSDictionary dictionaryWithObject:url forKey:@"url"];
             [routeWarningAlert show];
        } else if (self.jsonData.isFuzzyLocation) {
             routeWarningAlert = [[UIAlertView alloc] initWithTitle:title
                                                   message:LOCATION_NOT_ACCURATE
                                                  delegate:self
                                         cancelButtonTitle:cancel
                                         otherButtonTitles:confirm, nil];
             routeWarningAlert.userInfo = [NSDictionary dictionaryWithObject:url forKey:@"url"];
             [routeWarningAlert show];
        } else {
             normalRouteWarningAlert = [[UIAlertView alloc] initWithTitle:@"提醒您"
                                                           message:@"如攤販沒有出現在導引位置上，則可能在附近。座標位置如有誤差，歡迎提供更正確的資訊！"
                                                          delegate:self
                                                 cancelButtonTitle:cancel
                                                 otherButtonTitles:confirm, nil];
             normalRouteWarningAlert.userInfo = [NSDictionary dictionaryWithObject:url forKey:@"url"];
            [normalRouteWarningAlert show];
        }
    }
}

-(void) launchGoogleMap:(NSURL *)url {
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)clickFbLike:(id)sender {
    NSString * rid = self.jsonData.gidSting;
    // TODO support unlike feature in the future
    if([storage isLike:rid]==false){
        [PRNetworkManager addLikeNumber:self.jsonData.gid installationId:@"TODO"
                           complete:^(int num){
                               [storage setLike:rid enable:true];
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   [self updateFbNumber:num];
                                   [self.btnFbLike setEnabled:false];
                               });
                           }  error:nil];
    }
}

- (void) updateCheckinNum:(int) number {
    [self.numCheckIn setText:[[NSString alloc] initWithFormat:@"%d", number]];
}

- (void) updateFbNumber:(int) number {
    [self.numLike setText:[[NSString alloc] initWithFormat:@"%d",number]];
}

/**
 * A function for parsing URL parameters.
 */
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

- (IBAction)clickFbCheckin:(id)sender {
    NSString * mapUrl = [[NSString alloc] initWithFormat:@"https://maps.google.com/maps?q=%f,%f",self.jsonData.latitude,self.jsonData.longtitude];
    NSMutableDictionary *params =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     self.jsonData.title, @"name",
     self.jsonData.memo, @"caption",
     self.jsonData.story, @"description",
     self.jsonData.listImageUrl, @"picture",
     mapUrl, @"link",
     nil];
    
//    // Invoke the dialog
//    [FBWebDialogs presentFeedDialogModallyWithSession:nil
//                                           parameters:params
//                                              handler:
//     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//         if (error) {
//             // Error launching the dialog or publishing a story.
//             NSLog(@"Error publishing story.");
//         } else {
//             if (result == FBWebDialogResultDialogNotCompleted) {
//                 // User clicked the "x" icon
//                 NSLog(@"User canceled story publishing.");
//             } else {
//                 // Handle the publish feed callback
//                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
//                 if (![urlParams valueForKey:@"post_id"]) {
//                     // User clicked the Cancel button
//                     NSLog(@"User canceled story publishing.");
//                 } else {
//                     // User clicked the Share button
//                     [PRNetworkManager reportCheckin:self.jsonData.gid installationId:@"TODO" checkinId:[urlParams valueForKey:@"post_id"]
//                                            complete:^(int result){
//                                                checkInNumber++;
//                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                    [self updateCheckinNum:checkInNumber];
//                                                });
//                                            }
//                                            error:^(int error) {
//                                            }];
//                 }
//             }
//         }
//     }];
}

- (IBAction)clickReportIssue:(id)sender {
    [self launchMailApp:[NSArray arrayWithObjects:OFFICIAL_REPORT_EMAIL, nil] subject:@"問題回報"];
}

- (IBAction)clickBookmark:(id)sender {
    BOOL enable = [storage isFavorRecord:self.jsonData.gidSting] == false;
    [self updateBookmark:[storage setFavor:self.jsonData.gidSting json:[self.jsonData getOriginalJson] enable:enable]];
}

- (IBAction)clickAddMessage:(id)sender {
    if ([self.jsonData canPost] == FALSE || [self.jsonData isBigIssue] == FALSE) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"新增留言" message:@"請輸入留言" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"確定", nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"此類別不開放留言功能"
                                    message:@"此類別不開放留言功能，如有任何疑問，歡迎透過『回報資訊錯誤』告知我們！"
                                    delegate:self
                                    cancelButtonTitle:@"取消"
                          otherButtonTitles:nil] show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:true completion:nil];
}

#pragma mark -
// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action {
//    if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
//        [FBSession.activeSession requestNewPublishPermissions:@[@"publish_actions"]
//                                              defaultAudience:FBSessionDefaultAudienceFriends
//                                            completionHandler:^(FBSession *session, NSError *error) {
//                                                if (!error) {
//                                                    action();
//                                                }
//                                            }];
//    } else {
//        action();
//    }
    action();
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:true completion:^(void){
            [self launchMailApp:[NSArray arrayWithObjects:OFFICIAL_REPORT_EMAIL, nil]
                    subject:[[NSString alloc] initWithFormat:@"提供好事圖片 %@",_mTitle.text]
                      image:UIImageJPEGRepresentation([info objectForKey:UIImagePickerControllerOriginalImage],1.0)];
        }];
}

-(void)launchMailApp:(NSArray *)recipients subject:(NSString *)subject {
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:subject];
    [controller setToRecipients:recipients];
    if (controller){
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)launchMailApp:(NSArray *)recipients subject:(NSString *)subject image:(NSData *)image {
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:subject];
    [controller setToRecipients:recipients];
    [controller addAttachmentData:image mimeType:@"image/jpeg" fileName:@"my_image.jpg"];
    if (controller){
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error {
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if(buttonIndex == 0) {// from photo
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:true completion:nil];
    } else if(buttonIndex == 1) { //from camera
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;//TODO add error handling
        [self presentViewController:picker animated:true completion:nil];
    }
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if( [alertView isEqual:installGoogleMapAlert] ){
        switch (buttonIndex) {
            case 1:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/tw/app/id585027354?mt=8"]];
                break;
            case 0:
            default:
                break;
        }
    } else if ([alertView isEqual:normalRouteWarningAlert] || [alertView isEqual:routeWarningAlert]) {
        switch (buttonIndex) {
            case 1:
                [self launchGoogleMap:[alertView.userInfo objectForKey:@"url"]];
                break;
            case 0:
            default:
                break;
        }
    } else { // it is message box alert
        switch (buttonIndex) {
            case 1:{
                [PRNetworkManager postMessage:self.jsonData.gid installationId:@"TODO"
                                      message:[alertView textFieldAtIndex:0].text complete:^(int num) {
                                          [self requestUpdateChats];
                                      }
                                        error:^(int errorCode) {
                                            // TODO show error message
                                        }];
                break;
            }
            case 0:
            default:{
                break;
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PRChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    if (cell == nil) {
        cell = [[PRChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CELL_IDENTIFIER];
    }
    
    [cell setData:[datasource objectAtIndex:[indexPath row]] seq:[indexPath row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self tapMessageReadMore:[datasource objectAtIndex:[indexPath row]] sequence:[indexPath row]];
}

-(void)tapMessageReadMore:(NSDictionary *)message sequence:(NSInteger) seq{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[[NSString alloc] initWithFormat:@"#%d",seq]
                                                    message:[message objectForKey:JSON_KEY_MSG]
                                                   delegate:nil
                                          cancelButtonTitle:@"確認"
                                          otherButtonTitles:nil];
    [alert show];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (datasource == nil) ? 0 : [datasource count];
}


@end
