//
//  PRPageItemCell.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/15.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRPageItemCell.h"

@implementation PRPageItemCell

@synthesize mText = _mText;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
