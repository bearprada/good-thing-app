//
//  PRPrefStorage.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/18.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRStorageImpl.h"
@interface PRPrefStorage : NSObject<PRStorageImpl>

@end
