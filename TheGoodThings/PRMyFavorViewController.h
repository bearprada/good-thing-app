//
//  PRMyFavorViewController.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/19.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRMyFavorViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@end
