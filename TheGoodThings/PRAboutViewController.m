//
//  PRAboutViewController.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/13.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRAboutViewController.h"
#import "Constance.h"
#import "PRBackNavButton.h"

@interface PRAboutViewController ()

@end

@implementation PRAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [[[PRBackNavButton alloc] initWithTarget:self] generate:@selector(handleBack:)];
}

- (void)handleBack:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickFacebookPage:(id)sender {
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:OFFICAIL_FACEBOOK_PAGE_ID]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:OFFICAIL_FACEBOOK_PAGE_ID]];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:URL_OFFICAIL_FACEBOOK_PAGE]];
    }
}
@end
