//
//  PRDetailNavButton.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/26.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRDetailNavButton.h"
#import "Constance.h"
#import "PRStorageFactory.h"

@implementation PRDetailNavButton {
    UIViewController * mTarget;
}

-(id) initWithTarget:(UIViewController *)target{
    mTarget = target;
    return self;
}

- (UIBarButtonItem *) generateDetailBarItem{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0.0f, 0.0f, 40.0f, 40.0f)];
    [button addTarget:mTarget action:@selector(clickNavDetail:) forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_menu.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"btn_menu_selected.png"] forState:UIControlStateHighlighted];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

-(void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            if([[PRStorageFactory create] getFavorNum]>0) {
                [mTarget performSegueWithIdentifier:SEGUE_FAVOR_VIEW sender:mTarget];
            }else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"沒有最愛記錄"
                                                                message:@"您沒有任何一筆最愛的記錄"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            break;
        case 1:
            [mTarget performSegueWithIdentifier:SEGUE_ABOUT_VIEW sender:self];
            break;
        default:
            break;
    }
}

@end
