//
//  PRMainViewController.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/2.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRMainViewController : UIViewController<UIActionSheetDelegate>{
}

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnTopCover;
@property (weak, nonatomic) IBOutlet UILabel *textCoverContent;
@property (weak, nonatomic) IBOutlet UIImageView *imageCoverStory;

- (IBAction)clickTopCover:(id)sender;
- (IBAction)clickMainfood:(id)sender;
- (IBAction)clickSnack:(id)sender;
- (IBAction)clickFruit:(id)sender;
- (IBAction)clickOther:(id)sender;
- (IBAction)clickTBI:(id)sender;
- (IBAction)clickSearchNear:(id)sender;

@end