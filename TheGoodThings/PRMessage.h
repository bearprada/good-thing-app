//
//  PRMessage.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/9/14.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>

#define JSON_KEY_USERNAME @"userId"
#define JSON_KEY_TIMESTAMP @"time"
#define JSON_KEY_MSG @"message"
#define JSON_KEY_MSG_ID @"messageId"

@interface PRMessage : NSObject

@property (assign, nonatomic) long timestamp;
@property (assign, nonatomic) long messageId;
@property (assign, nonatomic) NSString * name;
@property (assign, nonatomic) NSString * message;

-(id)initWithDict:(NSDictionary *)data;
-(NSDictionary *) getOriginalData;

@end
