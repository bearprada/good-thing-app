//
//  PRImagePreviewViewController.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/20.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRImagePreviewViewController : UIViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (assign, nonatomic) NSArray * images;
@property (assign, nonatomic) int currentPage;
@end
