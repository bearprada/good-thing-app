//
//  PRMyFavorViewController.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/19.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRMyFavorViewController.h"
#import "PRMapItemCell.h"
#import "PRDetailViewController.h"
#import "PRStorageFactory.h"
#import "Constance.h"
#import "PRGoodThing.h"
#import "PRBackNavButton.h"

#define FAVOR_CELL_IDENTIFY @"favor_item"

@implementation PRMyFavorViewController{
    NSIndexPath * currentSelection;
    NSMutableArray * datasource;
    CLLocationManager *locationManager;
    CLLocation * currentLocation;
}

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = 77.0;
    self.clearsSelectionOnViewWillAppear = YES;
    datasource = [[PRStorageFactory create] getFavorList];
    self.navigationItem.leftBarButtonItem = [[[PRBackNavButton alloc] initWithTarget:self] generate:@selector(handleBack:)];

    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [self getCurrentLocation];
}

- (void)handleBack:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [datasource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PRMapItemCell *cell = [tableView dequeueReusableCellWithIdentifier:FAVOR_CELL_IDENTIFY];
    if (cell == nil) {
        cell = [[PRMapItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FAVOR_CELL_IDENTIFY];
    }
    PRGoodThing * goodThing = [datasource objectAtIndex:[indexPath row]];
    [cell updateView:goodThing location:currentLocation];
    return cell;
}

-(void) getCurrentLocation {
    [locationManager startUpdatingLocation];
    currentLocation = locationManager.location;
    [locationManager stopUpdatingLocation];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     currentSelection = indexPath;
    [self performSegueWithIdentifier:SEGUE_DETAIL_VIEW sender: self];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:SEGUE_DETAIL_VIEW] && currentSelection!=nil ){
        PRDetailViewController *controller =segue.destinationViewController;
        controller.jsonData = [datasource objectAtIndex:[currentSelection row]];
    }
}

@end
