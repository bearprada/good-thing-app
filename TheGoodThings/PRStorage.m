//
//  PRStorage.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/16.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRStorage.h"
#import "PRNetworkManager.h"
#import "PRGoodThing.h"
#define STORAGE_KEY @"__favor_record_key"
@implementation PRStorage{
    NSMutableDictionary * storage;
    NSMutableDictionary * internetCache;
    NSMutableDictionary * likeHistroyCache;
}

- (id)init{
    internetCache = [[NSMutableDictionary alloc] init];
    likeHistroyCache = [[NSMutableDictionary alloc] init];
    NSUserDefaults * userPrefs = [NSUserDefaults standardUserDefaults];
    NSData * data = [userPrefs objectForKey:STORAGE_KEY];
    if (data != nil) {
        NSLog(@" restore storage the data type : %@", [data class]);
        NSError * error = nil;
        NSDictionary * dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        if (error == nil) {
            storage = [[NSMutableDictionary alloc] initWithDictionary:dict];
            return self;
        }
    }
    storage = [[NSMutableDictionary alloc] init];
    
    return self;
}

-(BOOL)isFavorRecord:(NSString *)rid{
    return [storage objectForKey:[self mappingKey:rid]] != nil;
}

-(NSString *)mappingKey:(NSString *)rid {
    return [[NSString alloc] initWithFormat:@"id-%@",rid ];
}

-(BOOL)isLike:(NSString *)rid{
    return [likeHistroyCache objectForKey:rid] != nil;
}

-(void)setLike:(NSString *)rid enable:(BOOL)enable{
    if(enable){
        [likeHistroyCache setValue:@"TEST" forKey:rid];
    }else{
        [likeHistroyCache removeObjectForKey:rid];
    }
}

-(BOOL)setFavor:(NSString *)rid json:(NSDictionary *)data enable:(BOOL)enable {
    if(data==nil)
        return false;
    NSString * realId = [self mappingKey:rid];
    if(enable){
        [storage setValue:data forKey:realId];//FIXME let the value contain useful information
    }else{
        [storage removeObjectForKey:realId];
    }
    return enable;
}


-(void)getGoodThings:(int)catogoryId location:(CLLocation *)currentLocation complete:(void(^)(NSArray *))complete focusUpdate:(BOOL)focusUpdated {
    NSString *cid = [[NSString alloc] initWithFormat:@"%d",catogoryId];
    if(focusUpdated){
        [PRNetworkManager requestGoodThingList:catogoryId currentLocation:currentLocation complete:^(NSArray * json){
            [internetCache setValue:json forKey:cid];
            complete(json);
        }];
    }else{
        if([internetCache objectForKey:cid]==nil){
            [PRNetworkManager requestGoodThingList:catogoryId currentLocation:currentLocation complete:^(NSArray * json){
                [internetCache setValue:json forKey:cid];
                complete(json);
            }];
        }else{
            complete([internetCache objectForKey:cid]);
        }
    }
}

-(NSMutableDictionary *) _findData:(id)key {
    for(NSString * k in internetCache){
        for(NSMutableDictionary * v in [internetCache objectForKey:k]){
            if([v objectForKey:@"title"]==key){
                return v;
            }
        }
    }
    return nil;
}

-(NSMutableArray *)getFavorList {
    NSMutableArray * array = [[NSMutableArray alloc] init];
    for(NSString * k in storage){
        NSMutableDictionary * tmp = [storage objectForKey:k];
        if(tmp!=nil){
            [array addObject:[[PRGoodThing alloc] initWithJson:tmp]];
        }else{
            //TODO error handling if cannot found the detail in cache
        }
    }
    return array;
}

-(NSInteger)getFavorNum{
    return [[self getFavorList] count];
}

-(void)dispose{
    NSError * error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:storage
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    if (error!=nil) {
        NSLog(@"has some wrong %@", error);
    } else {
        NSUserDefaults * userPrefs = [NSUserDefaults standardUserDefaults];
        [userPrefs setObject:jsonData forKey:STORAGE_KEY];
        [userPrefs synchronize];
    }
}

@end
