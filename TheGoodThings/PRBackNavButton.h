//
//  PRBackNavButton.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/27.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRBackNavButton : NSObject
- (id) initWithTarget:(UIViewController *)target;
- (UIBarButtonItem *) generate:(SEL)selector;
@end
