//
//  PRStorageImpl.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/16.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol PRStorageImpl <NSObject>
@required
-(BOOL)isFavorRecord:(NSString *)rid;
-(BOOL)setFavor:(NSString *)rid json:(NSDictionary *)jsonData enable:(BOOL)enable;
-(BOOL)isLike:(NSString *)rid;
-(void)setLike:(NSString *)rid enable:(BOOL)enable;

-(void)getGoodThings:(int)catogoryId location:(CLLocation *)currentLocation complete:(void(^)(NSArray *))complete focusUpdate:(BOOL)focusUpdated;

-(NSMutableArray *)getFavorList;
-(NSInteger)getFavorNum;
-(void)dispose;

@end
