//
//  PRItemListViewController.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRItemListViewController.h"
#import "PRMapItemCell.h"
#import "PRDetailViewController.h"

#import <CoreLocation/CoreLocation.h>
#import "PRLocaltionCalculator.h"
#import "PRStorageFactory.h"
#import "PRStorageImpl.h"
#import "Constance.h"
#import "PRGoodThing.h"
#import "PRDetailNavButton.h"
#import "PRBackNavButton.h"

@interface PRItemListViewController () {
    PRDetailNavButton * mDetailNavButton;
}

@end

@implementation PRItemListViewController{
    CLLocationManager *locationManager;
    CLLocation * currentLocation;
    NSArray * dataSource;
    NSMutableArray * jsonDataArray;
    NSIndexPath * currentSelection;
    UIActivityIndicatorView * activityIndicator;
}

@synthesize catagoryId = _catagoryId;
@synthesize mSearchBar = _mSearchBar;
@synthesize catagoryName = _catagoryName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated{
    [self updateTable];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 80.0;

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc]
                                        init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:activityIndicator];
    activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    
    [activityIndicator startAnimating];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    
    jsonDataArray = [[NSMutableArray alloc] init];
    [self initTable];
    mDetailNavButton = [[PRDetailNavButton alloc] initWithTarget:self];
    self.navigationItem.rightBarButtonItem = [mDetailNavButton generateDetailBarItem];
    
    [self.mSearchBar setImage:[UIImage imageNamed: @"btn_searchbar_cancel.png"] forSearchBarIcon:UISearchBarIconClear state:UIControlStateNormal];

    self.navigationItem.leftBarButtonItem = [[[PRBackNavButton alloc] initWithTarget:self] generate:@selector(handleBack:)];
    self.navigationController.topViewController.title = self.catagoryName;
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations {
    currentLocation = locations[0];
    [locationManager stopUpdatingLocation];
    
    [self updateTable];
}

- (void)handleBack:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (void)clickNavDetail:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"好事地圖" delegate:mDetailNavButton cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"我的收藏", @"關於", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)initTable{
    [self _getJsonArray:false];
}

- (void) _getJsonArray:(BOOL)focusUpdate{
    int cid = (int)self.catagoryId;
    [[PRStorageFactory create] getGoodThings:cid location:currentLocation complete:^(NSArray * json){
        dataSource = json;
        [jsonDataArray removeAllObjects];
        [jsonDataArray addObjectsFromArray:dataSource];
        [self performSelectorOnMainThread:@selector(updateTable) withObject:nil waitUntilDone:NO];
    } focusUpdate:focusUpdate];
}

- (void)refreshTable{
    [self _getJsonArray:true];
}

- (void)updateTable{
    [self.tableView reloadData];
    if([self.refreshControl isRefreshing])
        [self.refreshControl endRefreshing];
    if([activityIndicator isAnimating])
        [activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PRMapItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[PRMapItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    PRGoodThing * goodThing = [jsonDataArray objectAtIndex:[indexPath row]];
    [cell updateView:goodThing location:currentLocation];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (jsonDataArray==nil) ? 0 : [jsonDataArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    currentSelection = indexPath;
    [self performSegueWithIdentifier:SEGUE_DETAIL_VIEW sender: self];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"show_detail"] && currentSelection!=nil ){
        PRDetailViewController *controller =segue.destinationViewController;
        controller.jsonData = [jsonDataArray objectAtIndex:[currentSelection row]];
    }
}

#pragma UISerachBarDelegate
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text{
    [jsonDataArray removeAllObjects]; // First clear the filtered array.
    if(text==nil||[text isEqual:@""]){
        [jsonDataArray addObjectsFromArray:dataSource];
    }else{
        for (PRGoodThing *d in dataSource){
            if ([d.title rangeOfString:text].location != NSNotFound){
                [jsonDataArray addObject:d];
            }
        }
    }
    [[self tableView] reloadData];
}

-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [jsonDataArray removeAllObjects];
    [jsonDataArray addObjectsFromArray:dataSource];
    [self.view endEditing:YES];
    [[self tableView] reloadData];
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.view endEditing:YES];
}

@end
