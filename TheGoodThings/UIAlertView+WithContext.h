//
//  UIAlertView+WithContext.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/26.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>
@interface UIAlertView (WithContext)
@property (nonatomic, copy) NSDictionary *userInfo;
@end
