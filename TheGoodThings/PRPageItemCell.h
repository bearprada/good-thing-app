//
//  PRPageItemCell.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/15.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRPageItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mText;

@end
