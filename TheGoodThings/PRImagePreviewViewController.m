//
//  PRImagePreviewViewController.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/20.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRImagePreviewViewController.h"
#import "PRBackNavButton.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Constance.h"

@interface PRImagePreviewViewController ()

@end

@implementation PRImagePreviewViewController

@synthesize mScrollView = _mScrollView;
@synthesize images = _images;
@synthesize currentPage = _currentPage;

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    CGFloat width = screenRect.size.width;
    CGFloat height = screenRect.size.height;
    for(int i=0;i<[self.images count];i++){
        CGRect frame;
        frame.origin.x = width*i;
        frame.origin.y = 0;
        frame.size = CGSizeMake(width,height);
        UIImageView * img = [[UIImageView alloc] initWithFrame:frame];
        [img sd_setImageWithURL:[NSURL URLWithString:[self.images objectAtIndex:i]]
                  placeholderImage:[UIImage imageNamed:PLACEHOLDER_ICON]];
        img.contentMode = UIViewContentModeScaleAspectFit;
        [self.mScrollView addSubview:img];
    }
    
    self.mScrollView.contentSize = CGSizeMake(width *[self.images count] , height);
    CGPoint pt;
    pt.x = width*self.currentPage;
    pt.y = 0;
    [self.mScrollView setContentOffset:pt animated:false];
    self.navigationItem.leftBarButtonItem = [[[PRBackNavButton alloc] initWithTarget:self] generate:@selector(handleBack:)];
}

- (void)handleBack:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
