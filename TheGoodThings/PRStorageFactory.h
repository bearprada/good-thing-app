//
//  PRStorageFactory.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/16.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRStorageImpl.h"

@interface PRStorageFactory : NSObject

+(id<PRStorageImpl>)create;

@end
