//
//  PRNetworkManager.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/15.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRNetworkManager.h"
#import "Constance.h"
#import "PRGoodThing.h"
#import "PRMessage.h"

@implementation PRNetworkManager

static NSOperationQueue * queue;

+(void) initialize{
    queue = [[NSOperationQueue alloc] init];
}

+(NSArray *) sorting:(NSArray *)input location:(CLLocation *)location {
    return [input sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        double x = [(PRGoodThing *)a getDistance:location];
        double y = [(PRGoodThing *)b getDistance:location];
        if (x == y) {
            return NSOrderedSame;
        } else if (x < y) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
    }];
}

+(void)requestGoodThingList:(int) catagoryId currentLocation:(CLLocation *)location complete:(void(^)(NSArray *))completeBlock{
    NSString * paramType = (catagoryId==ALL_CATAGORY_ID) ? @"" : [[NSString alloc] initWithFormat:@"type=%d",catagoryId];
    NSString * paramGeoLocation = (location == nil) ? @"" : [[NSString alloc] initWithFormat:@"lat=%f&lon=%f",location.coordinate.latitude,location.coordinate.longitude];
    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?%@&%@",GOOD_THING_SERVER_URL,GOOD_THING_LIST,paramType,paramGeoLocation];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200){
            NSError *e = nil;
            //TODO json's error handling
            NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if(completeBlock!=nil){
                NSArray * jsonArr = [json objectForKey:@"results"];
                NSMutableArray * result = [[NSMutableArray alloc] initWithCapacity:[jsonArr count]];
                for (int i=0 ; i<[jsonArr count] ; i++) {
                    [result addObject:[[PRGoodThing alloc] initWithJson:[jsonArr objectAtIndex:i]]];
                }
                jsonArr = nil;
                completeBlock([self sorting:result location:location]);
            }
        }else{
            NSLog(@"error request : %d",(int)[httpResponse statusCode]);
        }
    }];
}

+(void)requestTopStory:(CLLocation *)location complete:(void(^)(NSDictionary *))completeBlock{
    NSString * url;
    if (location==nil ) {
        url = [[NSString alloc] initWithFormat:@"%@%@",GOOD_THING_SERVER_URL,GOOD_THING_COVER_STORY];
    }else {
        url = [[NSString alloc] initWithFormat:@"%@%@?lat=%f&lon=%f",GOOD_THING_SERVER_URL,GOOD_THING_COVER_STORY,location.coordinate.latitude,location.coordinate.longitude];
    }
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200){
            NSError *e = nil;
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if(completeBlock!=nil){
                completeBlock([json objectForKey:@"result"]);
            }
        }else{
            NSLog(@"error request : %d",(int)[httpResponse statusCode]);
        }
    }];
}

+(void)requestCheckinNumber:(int)rid complete:(void(^)(int number))completeBlock {
    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?rid=%d",GOOD_THING_SERVER_URL,GOOD_THING_GET_CHECKIN,rid];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200){
            NSError *e = nil;
            NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if(completeBlock!=nil){
                completeBlock([[json objectForKey:@"result"] intValue]);
            }
        }else{
            NSLog(@"error request : %d",(int)[httpResponse statusCode]);
        }
    }];
}

+(void)requestLikeNumber:(int)rid complete:(void(^)(int number))completeBlock {
    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?rid=%d",GOOD_THING_SERVER_URL,GOOD_THING_GET_LIKE,rid];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200){
            NSError *e = nil;
            //TODO json's error handling
            NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if(completeBlock!=nil){
                completeBlock([[json objectForKey:@"result"] intValue]);
            }
        }else{
            NSLog(@"error request : %d",(int)[httpResponse statusCode]);
        }
    }];
}

+(void)reportCheckin:(int)rid installationId:(NSString *)installationId checkinId:(NSString*)checkinId complete:(void(^)(int number))completeBlock error:(void(^)(int))errorBlock {
    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?rid=%d&uid=%@&cid=%@",GOOD_THING_SERVER_URL,GOOD_THING_ADD_CHECKIN,rid, installationId, checkinId];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200){
            NSError *e = nil;
            NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if( e!=nil && errorBlock!=nil ) errorBlock([e code]);
            else if(completeBlock!=nil) completeBlock([[json objectForKey:@"result"] intValue]);
        }else{
            if(errorBlock!=nil) errorBlock((int)[httpResponse statusCode]);
        }
    }];
    
}

+(void)addLikeNumber:(int)rid installationId:(NSString *)installationId
            complete:(void(^)(int number))completeBlock error:(void(^)(int))errorBlock{
    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?rid=%d&uid=%@",GOOD_THING_SERVER_URL,GOOD_THING_ADD_LIKE,rid,installationId];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];

    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200){
            NSError *e = nil;
            NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if( e!=nil && errorBlock!=nil ) errorBlock([e code]);
            else if(completeBlock!=nil) completeBlock([[json objectForKey:@"result"] intValue]);
        }else{
            if(errorBlock!=nil) errorBlock((int)[httpResponse statusCode]);
        }
    }];
}

+(void)requestPosts:(int)rid complete:(void(^)(NSArray * posts))completeBlock error:(void(^)(NSString * err))errorBlock{
    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?rid=%d", GOOD_THING_SERVER_URL, GOOD_THING_GET_MESSAGE, rid];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200) {
            NSError *e = nil;
            NSArray * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
//            NSMutableArray * result = [[NSMutableArray alloc] initWithCapacity:[json count]];
//            for(NSDictionary * d in json) {
//                [result addObject:[[PRMessage alloc] initWithDict:d]];
//            }
            if( e!=nil && errorBlock!=nil ) errorBlock(e.localizedFailureReason);
//            else if(completeBlock!=nil)     completeBlock(result);
            else if(completeBlock!=nil)     completeBlock(json);
        } else {
            if(errorBlock!=nil) errorBlock(@"網路發生問題");
        }
    }];
}

+(void)postMessage:(int)rid installationId:(NSString *)installationId message:(NSString *)msg
          complete:(void(^)(int number))completeBlock error:(void(^)(int errorCode))errorBlock {
    NSString *escapedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                  NULL,
                                                                                  (CFStringRef)msg,
                                                                                  NULL,
                                                                                  CFSTR("!*'();:@&=+$,/?%#[]"),
                                                                                  kCFStringEncodingUTF8));

    NSString * url = [[NSString alloc] initWithFormat:@"%@%@?rid=%d&uid=%@&message=%@", GOOD_THING_SERVER_URL, GOOD_THING_POST_MESSAGE, rid, installationId, escapedString];
    NSURLRequest * req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:req queue:queue completionHandler:^(NSURLResponse * response, NSData * data, NSError * err){
        NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
        if ([httpResponse statusCode]==200) {
            NSError *e = nil;
            NSMutableDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
            if( e!=nil && errorBlock!=nil ) errorBlock([e code]);
            else if(completeBlock!=nil)     completeBlock([[json objectForKey:@"result"] intValue]);
        } else {
            if(errorBlock!=nil) errorBlock((int)[httpResponse statusCode]);
        }
    }];

    
}


@end
