//
//  PRStorage.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/16.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRStorageImpl.h"

@interface PRStorage : NSObject<PRStorageImpl>
@end
