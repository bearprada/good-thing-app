//
//  PRMapItemCell.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "PRGoodThing.h"

@interface PRMapItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mTitle;
@property (weak, nonatomic) IBOutlet UIImageView *mImage;

@property (weak, nonatomic) IBOutlet UILabel *mDistance;
@property (weak, nonatomic) IBOutlet UIButton *mBookmarkBtn;
@property (weak, nonatomic) IBOutlet UITextView *mAddress;

- (IBAction)clickBookmark:(id)sender;

- (void)updateView:(PRGoodThing *)data location:(CLLocation *)currentLocation;

@end
