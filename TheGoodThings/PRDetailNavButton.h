//
//  PRDetailNavButton.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/26.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRDetailNavButton : NSObject<UIActionSheetDelegate>

-(id) initWithTarget:(UIViewController *)target;
- (UIBarButtonItem *) generateDetailBarItem;
@end
