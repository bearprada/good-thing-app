//
//  NearbyPlacesRequestResult.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/29.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "NearbyPlacesRequestResult.h"

@implementation NearbyPlacesRequestResult

- (id) initializeWithDelegate:(id)delegate {
    _nearbyPlacesRequestDelegate = delegate;
    return self;
}
/**
 * FBRequestDelegate
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
    NSArray *placesArray = [result objectForKey:@"data"];
    [_nearbyPlacesRequestDelegate nearbyPlacesRequestCompletedWithPlaces: placesArray];
}

- (void)request:(FBRequest*)request didFailWithError:(NSError*)error {
    NSLog(@"NearbyPlaces %@", [error localizedDescription]);
    [_nearbyPlacesRequestDelegate nearbyPlacesRequestFailed];
}

@end
