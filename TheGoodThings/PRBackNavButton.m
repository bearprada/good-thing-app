//
//  PRBackNavButton.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/27.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRBackNavButton.h"

@implementation PRBackNavButton {
    UIViewController * mTarget;
}

- (id) initWithTarget:(UIViewController *)target {
    mTarget = target;
    return self;
}

- (UIBarButtonItem *) generate:(SEL)selector {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(0.0f, 0.0f, 80.0f, 40.0f)];
    [button addTarget:mTarget action:selector forControlEvents:UIControlEventTouchUpInside];
    [button setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}

@end
