//
//  main.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PRAppDelegate class]));
    }
}
