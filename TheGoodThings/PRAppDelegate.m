//
//  PRAppDelegate.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/1.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRAppDelegate.h"
#import "Constance.h"
#import "PRStorageFactory.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation PRAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
     UIRemoteNotificationTypeAlert|
     UIRemoteNotificationTypeSound];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f) {
        [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0xE21A49)];
    } else {
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbg.png"] forBarMetrics:UIBarMetricsDefault];
    }
//    [FBSession activeSession];
//    [Flurry setCrashReportingEnabled:YES];
    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
//    [Flurry startSession:@"M9B6N2777Q753XWM7MM5"];
    [PRStorageFactory create]; // initialize storage
    
    /**
     See this note in https://developers.facebook.com/docs/tutorial/iossdk/upgrading-from-3.0-to-3.1/, section "Asking for Read & Write Permissions Separately". Even though this applies to the Facebook SDK for iOS it is an extension of the same requirement in the built-in framework.
    */
//    NSArray * permission = [[NSArray alloc] initWithObjects:@"email", nil];
//    [FBSession openActiveSessionWithPublishPermissions:permission defaultAudience:FBSessionDefaultAudienceOnlyMe allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//        if(error != nil) {
//            NSLog(@" refresh facebook auth error %@", error);
//        }
//    }];
    return YES;
}

#pragma mark CrittercismDelegate
- (void)crittercismDidCrashOnLastLoad {
    
}

// FBSample logic
// In the login workflow, the Facebook native application, or Safari will transition back to
// this applicaiton via a url following the scheme fb[app id]://; the call to handleOpenURL
// below captures the token, in the case of success, on behalf of the FBSession object
//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    return [FBAppCall handleOpenURL:url
//                  sourceApplication:sourceApplication
//                        withSession:[FBSession activeSession]];
//}

//- (void)applicationDidBecomeActive:(UIApplication *)application {
//    [FBAppEvents activateApp];
//    [FBAppCall handleDidBecomeActive];
//}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    NSLog(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"applicationDidEnterBackground");
    [[PRStorageFactory create] dispose];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"applicationWillEnterForeground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"applicationWillTerminate");
    [[PRStorageFactory create] dispose];
//    [FBSession.activeSession close];
}

@end
