//
//  PRPrefStorage.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/18.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRPrefStorage.h"
#import "PRNetworkManager.h"

#define STORAGE_PREFIX @"__good_thing_"
#define STORAGE_LIKE_PREFIX @"__good_like_cf"

@implementation PRPrefStorage{
    NSUserDefaults *userPrefs;
    NSMutableDictionary * internetCache;
}

-(id) init {
    userPrefs = [NSUserDefaults standardUserDefaults];
    internetCache = [[NSMutableDictionary alloc] init];
    return self;
}

-(BOOL)isFavorRecord:(NSString *)rid{
    return [userPrefs objectForKey:[self convertKey:rid]] != nil;
}

-(NSString *)convertKey:(NSString *)rid{
    return [[NSString alloc] initWithFormat:@"%@%@",STORAGE_PREFIX,rid ];
}

-(NSString *)convertLikeKey:(NSString *)rid{
    return [[NSString alloc] initWithFormat:@"%@%@",STORAGE_LIKE_PREFIX,rid ];
}

-(BOOL)setFavor:(NSString *)rid json:(NSDictionary *)jsonData enable:(BOOL)enable {
    if(enable){
        [userPrefs setObject:jsonData forKey:rid];
    }else{
        [userPrefs removeObjectForKey:rid];
    }
    [userPrefs synchronize];
    return enable;
}


-(void)getGoodThings:(int)catogoryId location:(CLLocation *)currentLocation complete:(void(^)(NSArray *))complete focusUpdate:(BOOL)focusUpdated {
    NSString *cid = [[NSString alloc] initWithFormat:@"%d",catogoryId];
    if(focusUpdated){
        [PRNetworkManager requestGoodThingList:catogoryId currentLocation:nil complete:^(NSArray * json){
            [internetCache setValue:json forKey:cid];
            complete(json);
        }];
    }else{
        if([internetCache objectForKey:cid]==nil){
            [PRNetworkManager requestGoodThingList:catogoryId currentLocation:nil complete:^(NSArray * json){
                [internetCache setValue:json forKey:cid];
                complete(json);
            }];
        }else{
            complete([internetCache objectForKey:cid]);
        }
    }
}

-(BOOL)isLike:(NSString *)rid{
    return [userPrefs objectForKey:[self convertLikeKey:rid]]!=nil;
}

-(void)setLike:(NSString *)rid enable:(BOOL)enable{
    if(enable){
        [userPrefs setObject:@"" forKey:[self convertLikeKey:rid]];
    }else{
        [userPrefs removeObjectForKey:rid];
    }
    [userPrefs synchronize];
}

-(NSMutableArray *)getFavorList{
    //TODO bug fix .... the wrong ids
    NSMutableArray * array = [[NSMutableArray alloc] init];
    for(NSString *k in [userPrefs dictionaryRepresentation]){
        if([k hasPrefix:STORAGE_PREFIX]){
            [array addObject:[userPrefs objectForKey:k]];
        }
    }
    return array;
}

-(NSInteger)getFavorNum{
    NSInteger count = 0;
    for(NSString *k in [userPrefs dictionaryRepresentation]){
        if([k isKindOfClass:[NSString class]] && [k hasPrefix:STORAGE_PREFIX]){
            count ++;
        }
    }
    return count;
}

-(void)dispose{
    //TODO
}

@end
