//
//  PRLocaltionCalculator.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/15.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface PRLocaltionCalculator : NSObject

+ (double) calDistance:(CLLocation *)a b:(CLLocation *)b;
+ (NSString *) distance2string:(double) distance;

@end
