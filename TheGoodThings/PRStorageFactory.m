//
//  PRStorageFactory.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/16.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRStorageFactory.h"
#import "PRStorage.h"
#import "PRPrefStorage.h"

@implementation PRStorageFactory
static id<PRStorageImpl> instance;
+(id<PRStorageImpl>)create;{
    if(instance==nil)
        //instance = [[PRPrefStorage alloc] init];
        instance = [[PRStorage alloc] init];
    return instance;
}
@end