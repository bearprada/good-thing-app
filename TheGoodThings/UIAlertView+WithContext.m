//
//  UIAlertView+WithContext.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/26.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "UIAlertView+WithContext.h"
// This enum is actually declared elseware
@implementation UIAlertView (Context)
static char ContextPrivateKey;
-(void)setUserInfo:(NSDictionary *)userInfo{
    objc_setAssociatedObject(self, &ContextPrivateKey, userInfo, 3);
}

-(NSDictionary *)userInfo{
    return objc_getAssociatedObject(self, &ContextPrivateKey);
}
@end
