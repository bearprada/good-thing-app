//
//  PRChatCell.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/9/13.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PRMessage.h"

@interface PRChatCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *mUsername;
@property (weak, nonatomic) IBOutlet UILabel *mTime;
@property (weak, nonatomic) IBOutlet UILabel *mMessage;

// FIXME current implementation will not has the username, so we use the uuid that generate by Parse.com.
// for UX friendly we put the sequence number in v1.0.1
-(void) setData:(NSDictionary *)message seq:(int)seq;

@end
