//
//  PRUtils.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/22.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PRUtils : NSObject

+ (UIBarButtonItem *) genBackBarItem:(id)target selector:(SEL)selector;
@end
