//
//  NearbyPlacesRequestResult.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/29.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NearbyPlacesRequestDelegate;
@interface NearbyPlacesRequestResult : NSObject {
    id _nearbyPlacesRequestDelegate;
}
- (id) initializeWithDelegate: (id)delegate;

@end

@protocol NearbyPlacesRequestDelegate
- (void) nearbyPlacesRequestCompletedWithPlaces:(NSArray *)placesArray;
- (void) nearbyPlacesRequestFailed;
@end
