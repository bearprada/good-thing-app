//
//  Constance.h
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/30.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#ifndef TheGoodThings_Constance_h
#define TheGoodThings_Constance_h

#define JSON_KEY_TITLE @"title"
#define JSON_KEY_STORY @"story"
#define JSON_KEY_MEMO @"memo"
#define JSON_KEY_ADDRESS @"address"
#define JSON_KEY_IMG_URL @"imageUrl"
#define JSON_KEY_LIST_IMG_URL @"list_image_url"
#define JSON_KEY_DETAIL_IMG_URL @"detail_image_url"
#define JSON_KEY_IS_TBI @"is_big_issue"
#define JSON_KEY_CAN_POST @"can_post"

#define JSON_KEY_LAT @"latitude"
#define JSON_KEY_LON @"longtitude"
#define JSON_KEY_IMGS @"images"
#define JSON_KEY_BUSINESS_TIME @"business_time"
#define JSON_KEY_ID @"gid"

#define SEGUE_LIST_VIEW @"move_to_list_view"
#define SEGUE_DETAIL_VIEW @"show_detail"
#define SEGUE_FAVOR_VIEW @"show_my_favor"
#define SEGUE_ABOUT_VIEW @"move_to_about_page"

#define IMG_BOOKMARK_ENABLE @"btn_favor_enable"
#define IMG_BOOKMARK_DISABLE @"btn_favor_disable"

#define DEFAULT_DISPLAY_IMAGE_NUM 5

#define IMG_ADD_PHOTO @"btn_new_image.png"

#define OFFICAIL_FACEBOOK_PAGE_ID @"fb://profile/660640677293643"
#define URL_OFFICAIL_FACEBOOK_PAGE @"http://www.facebook.com/GoodMaps2013"

#define GOOD_THING_SERVER_URL @"http://goodthing.tw:8080/good_thing/"
//#define GOOD_THING_SERVER_URL @"http://192.168.0.106:8080/good-thing-server/"
#define GOOD_THING_LIST @"mobile/findGoodThings"
#define GOOD_THING_COVER_STORY @"mobile/findTopStory"
#define GOOD_THING_ADD_LIKE @"mobile/addLike"
#define GOOD_THING_GET_LIKE @"mobile/getLikeNum"
#define GOOD_THING_POST_MESSAGE @"mobile/post"
#define GOOD_THING_GET_MESSAGE @"mobile/listPosts"
#define GOOD_THING_ADD_CHECKIN @"mobile/addCheckin"
#define GOOD_THING_GET_CHECKIN @"mobile/getCheckinNum"

#define OFFICIAL_REPORT_EMAIL @"goodmaps2013@gmail.com"

#define ALL_CATAGORY_ID 9999999

#define PLACEHOLDER_COVER @"placeholder_cover.png"
#define PLACEHOLDER_DETAIL_COVER @"placeholder_detail_cover.png"
#define PLACEHOLDER_ICON @"placeholder_icon.png"

#define PLACEHOLDER_ICON @"placeholder_icon1.png"
#define PLACEHOLDER_ICON @"placeholder_icon2.png"
#define PLACEHOLDER_ICON @"placeholder_icon3.png"


#define DATA_NOT_ACCURATE @"此攤販尚無詳細資料，歡迎網友提供更多資訊。好的"
#define LOCATION_NOT_ACCURATE @"若攤販無確切地址，則導引座標為大概位置。\n歡迎網友供更正確的資訊！"

#endif
