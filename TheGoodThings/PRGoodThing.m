//
//  PRGoodThing.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/8/21.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRGoodThing.h"
#import "Constance.h"
#import "PRLocaltionCalculator.h"

@implementation PRGoodThing {
    NSDictionary * originalJson;
}

@synthesize title = _title;
@synthesize memo = _memo;
@synthesize story = _story;
@synthesize gid = _gid;
@synthesize address = _address;
@synthesize distance = _distance;
@synthesize titleImageUrl = _titleImageUrl;
@synthesize coverImageUrl = _coverImageUrl;
@synthesize listImageUrl = _listImageUrl;
@synthesize latitude = _latitude;
@synthesize longtitude = _longtitude;
@synthesize images = _images;
@synthesize isBigIssue = _isBigIssue;
@synthesize canPost = _canPost;
@synthesize businessTime = _businessTime;
@synthesize isFuzzyLocation = _isFuzzyLocation;

-(id)initWithJson:(NSDictionary *)json{
    originalJson = json;
    
    self.title = [json objectForKey:JSON_KEY_TITLE];
    self.address = [json objectForKey:JSON_KEY_ADDRESS];
    self.story = [json objectForKey:JSON_KEY_STORY];
    self.memo = [json objectForKey:JSON_KEY_MEMO];
    self.gid = [[json objectForKey:JSON_KEY_ID] longValue];
    self.gidSting = [json objectForKey:JSON_KEY_ID];
    self.titleImageUrl = [json objectForKey:JSON_KEY_IMG_URL];

    self.listImageUrl = [self getValue:json key:JSON_KEY_LIST_IMG_URL defaultValue:self.titleImageUrl];
    self.coverImageUrl = [self getValue:json key:JSON_KEY_DETAIL_IMG_URL defaultValue:self.titleImageUrl];

    self.businessTime = [json objectForKey:JSON_KEY_BUSINESS_TIME];
    self.latitude = [[json objectForKey:JSON_KEY_LAT] doubleValue];
    self.longtitude = [[json objectForKey:JSON_KEY_LON] doubleValue];
    self.images = [json objectForKey:JSON_KEY_IMGS];
    
    // FIXME make sure the cast issue
    self.isBigIssue = [[self getValue:json key:JSON_KEY_IS_TBI
                         defaultValue:[NSNumber numberWithBool:TRUE]] boolValue];
    self.canPost = [[self getValue:json key:JSON_KEY_CAN_POST
                      defaultValue:[NSNumber numberWithBool:FALSE]] boolValue];
    return self;
}

-(id) getValue:(NSDictionary *)data key:(NSString *)key defaultValue:(id)defaultValue {
    id value = [data valueForKey:key];
    return (value != nil) ? value : defaultValue;
}

-(NSString *)story {
    return ( _story == nil || [_story isEqualToString:@""] ) ?DATA_NOT_ACCURATE : _story;
}

-(double) getDistance:(CLLocation *)location {
    CLLocation * b = [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longtitude];
    return [PRLocaltionCalculator calDistance:location b:b];
}

-(NSString *)memo {
    return ( _memo == nil || [_memo isEqualToString:@""] ) ? DATA_NOT_ACCURATE : _memo;
}

-(NSDictionary *)getOriginalJson {
    return originalJson;
}

- (BOOL) isFuzzyLocation {
    return (self.latitude == 0 && self.longtitude == 0);
}

@end
