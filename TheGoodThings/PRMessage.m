//
//  PRMessage.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/9/14.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRMessage.h"

@implementation PRMessage {
    NSDictionary * originalData;
}
@synthesize name = _name;
@synthesize message = _message;
@synthesize timestamp = _timestamp;
@synthesize messageId = _messageId;

-(id)initWithDict:(NSDictionary *)data{
    originalData = data;
    self.name = [data objectForKey:JSON_KEY_USERNAME];
    self.timestamp = [[data objectForKey:JSON_KEY_TIMESTAMP] longValue];
    self.message = [data objectForKey:JSON_KEY_MSG];
    self.messageId = [[data objectForKey:JSON_KEY_MSG_ID] longValue];
    return self;
}
-(NSDictionary *) getOriginalData {
    return originalData;
}
@end
