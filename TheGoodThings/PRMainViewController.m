//
//  PRMainViewController.m
//  TheGoodThings
//
//  Created by PRADA Hsiung on 13/6/2.
//  Copyright (c) 2013年 PRADA. All rights reserved.
//

#import "PRMainViewController.h"
#import "PRPageItemCell.h"
#import "PRDetailViewController.h"
#import "PRItemListViewController.h"
#import "PRNetworkManager.h"
#import "PRStorage.h"
#import "PRStorageFactory.h"
#import "PRMyFavorViewController.h"
#import "Constance.h"
#import "PRGoodThing.h"
#import "PRDetailNavButton.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PRMainViewController ()

@end

@implementation PRMainViewController{
    PRGoodThing * topStoryJson;
    PRDetailNavButton * mDetailNavButton;
    int selectedCatogory;
    
}

@synthesize btnTopCover = _btnTopCover;
@synthesize textCoverContent = _textCoverContent;
@synthesize imageCoverStory = _imageCoverStory;
@synthesize mScrollView = _mScrollView;

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.btnTopCover setEnabled:false];
    //TODO reload it in the period time
    [PRNetworkManager requestTopStory:nil complete:^(NSDictionary * json){
        topStoryJson = [[PRGoodThing alloc] initWithJson:json];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            [self.textCoverContent setText:topStoryJson.story];
            [self.btnTopCover setEnabled:true];
            [self.imageCoverStory sd_setImageWithURL:[NSURL URLWithString:topStoryJson.titleImageUrl] placeholderImage:[UIImage imageNamed:PLACEHOLDER_COVER]];
        }];
    }];
    mDetailNavButton = [[PRDetailNavButton alloc] initWithTarget:self];
    self.navigationItem.rightBarButtonItem = [mDetailNavButton generateDetailBarItem];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.mScrollView.contentOffset = CGPointMake(0, 0);
    [self.mScrollView setContentOffset:CGPointZero animated:NO];
}

- (void)clickNavDetail:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"好事地圖" delegate:mDetailNavButton cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"我的收藏", @"關於", nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (NSString *)applicationDocumentsDirectory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:SEGUE_LIST_VIEW] && selectedCatogory > 0 ){
        PRItemListViewController *controller =segue.destinationViewController;
        controller.catagoryId = selectedCatogory;
        NSString * title;
                switch (selectedCatogory) {
                    case 1:
                        title = @"主食";
                        break;
                    case 2:
                        title = @"小吃";
                        break;
                    case 3:
                        title = @"冰品/水果";
                        break;
                    case 4:
                        title = @"其他";
                        break;
                    case 5:
                        title = @"大誌雜誌";
                        break;
                    case 6:
                        controller.catagoryId = ALL_CATAGORY_ID;
                        title = @"綜合搜尋";
                        break;
                    default:
                        break;
                }
        controller.catagoryName = title;
    } else if([segue.identifier isEqualToString:SEGUE_DETAIL_VIEW] ){
        PRDetailViewController *controller =segue.destinationViewController;
        controller.jsonData = topStoryJson;
    }
}

- (IBAction)clickTopCover:(id)sender {
    [self performSegueWithIdentifier:SEGUE_DETAIL_VIEW sender: self];
}

- (IBAction)clickMainfood:(id)sender {
    selectedCatogory = 1;
    [self performSegueWithIdentifier:SEGUE_LIST_VIEW sender: self];
}

- (IBAction)clickSnack:(id)sender {
    selectedCatogory = 2;
    [self performSegueWithIdentifier:SEGUE_LIST_VIEW sender: self];
}

- (IBAction)clickFruit:(id)sender {
    selectedCatogory = 3;
    [self performSegueWithIdentifier:SEGUE_LIST_VIEW sender: self];
}

- (IBAction)clickOther:(id)sender {
    selectedCatogory = 4;
    [self performSegueWithIdentifier:SEGUE_LIST_VIEW sender: self];
}

- (IBAction)clickTBI:(id)sender {
    selectedCatogory = 5;
    [self performSegueWithIdentifier:SEGUE_LIST_VIEW sender: self];
}

- (IBAction)clickSearchNear:(id)sender {
    selectedCatogory = 6;
    [self performSegueWithIdentifier:SEGUE_LIST_VIEW sender: self];
}



@end
